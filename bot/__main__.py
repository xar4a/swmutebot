from bot.utils import *
from bot import data


start_bot()

client.parse_mode = 'html'


@client.on(events.NewMessage(pattern=command("swmute")))
async def handler(event):
    sender = event.sender.id
    reply = await event.get_reply_message()
    purgemastercheck = purgemasters.get_or_none(id=sender)
    chat_id = event.chat.id 
    args = get_args(event)

    if purgemastercheck is None:
        return await event.reply("<strong>У тебя нет прав!!</strong>")

    if not reply:
        if args:
            try:
                swmuteuser = (await client.get_entity(args[0])).id
                await getsw(event, chat_id, swmuteuser, sender)
            except AttributeError:
                await event.reply("Не нашел такого булата!")
        else:
            return await event.reply("Сделай реплай блять")

    await getsw(event, chat_id, reply.sender.id, event.sender.id)


@client.on(events.NewMessage(pattern=command("swall")))
async def handler(event):
    sender = event.sender.id
    chat_id = event.chat.id
    spurgemastercheck = sadms.get_or_none(id=sender)
    if spurgemastercheck is None:
        return await event.reply("<strong>У тебя нет прав!!</strong>")

    await swall(event, chat_id, sender)


@client.on(events.NewMessage(pattern=command("unmuteall")))
async def handler(event):
    sender = event.sender.id
    chat_id = event.chat.id
    purgemastercheck = purgemasters.get_or_none(id=sender)

    if purgemastercheck is None:
        return await event.reply("<strong>У тебя нет прав!!</strong>")

    if chat_id in data.swmuted:
        del data.swmuted[chat_id]

    await event.reply("<strong>Снял свмут у всех</strong>")


@client.on(events.NewMessage(pattern=command("cum")))
async def handler(event):
    sender = event.sender_id
    purgemastercheck = purgemasters.get_or_none(id=sender)
    if purgemastercheck is None:
        await event.reply("<strong> У тебя нет прав!! </strong>")
    else:
        await purge.main(event)


@client.on(events.NewMessage(pattern=command("getadm")))
async def handler(event):
    sender = event.sender_id
    reply = await event.get_reply_message()
    if not reply:
        args = get_args(event)
        if args:
            try:
                nadmin = (await client.get_entity(args[0])).id
                await purgepromote(event, nadmin, sender)
            except AttributeError:
                await event.reply("Не нашел такого булата!")
        else:
            return await event.reply("Где реплай сука")
    user = reply.sender.id
    await purgepromote(event, user, sender)


@client.on(events.NewMessage(pattern=command("deadm")))
async def handler(event):
    sender = event.sender_id
    reply = await event.get_reply_message()
    if not reply:
        if args:
            try:
                deadmin = (await client.get_entity(args[0])).id
                await purgemasterdemote(event, deadmin, sender)
            except AttributeError:
                await event.reply("Не нашел такого булата!")
        else:
            return await event.reply("Где реплай сука")
    user = reply.sender.id
    await purgemasterdemote(event, user, sender)


@client.on(events.NewMessage(pattern=command("getsadm")))
async def handler(event):
    sender = event.sender_id
    reply = await event.get_reply_message()
    if not reply:
        args = get_args(event)
        if args:
            try:
                nadmin = (await client.get_entity(args[0])).id
                await sadmpromote(event, nadmin, sender)
            except AttributeError:
                await event.reply("Не нашел такого булата!")
        else:
            return await event.reply("Где реплай сука")
    user = reply.sender.id
    await sadmpromote(event, user, sender)


@client.on(events.NewMessage(pattern=command("desadm")))
async def handler(event):
    sender = event.sender_id
    args = get_args(event)
    reply = await event.get_reply_message()
    if not reply:
        if args:
            try:
                deadmin = (await client.get_entity(args[0])).id
                await sadmdemote(event, deadmin, sender)
            except AttributeError:
                await event.reply("Не нашел такого булата!")
        else:
            return await event.reply("Где реплай сука")
    user = reply.sender.id
    await sadmdemote(event, user, sender)


@client.on(events.NewMessage(func=lambda e: e.chat.id in data.swmuted))
async def handler(event):
    if event.sender.id in data.swmuted[event.chat.id]:
        await event.delete()
    if 1337 in data.swmuted[event.chat.id]:
        await event.delete()


if __name__ == '__main__':
    logging.info("===> {} Connection: {}".format(datetime.now().time(), "Bot started!"))
    client.run_until_disconnected()
