from bot.db import *


async def purgemasterdemote(event, user, sender):
    admincheck = sadms.get_or_none(id=sender)
    if admincheck is None:  
        return await event.reply("Ты не админ!")
    
    purgemasters.delete().where(purgemasters.id == user).execute()
    await event.reply("Снят админ!")

async def sadmdemote(event, user, sender):
    admincheck = sadms.get_or_none(id=sender)
    if admincheck is None:  
        return await event.reply("Ты не админ!")
    
    sadms.delete().where(sadms.id == user).execute()
    await event.reply("Снят <strong>супер</strong> админ!")
