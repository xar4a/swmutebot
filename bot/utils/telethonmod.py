from telethon import TelegramClient, events, errors
from bot.utils.argparsemod import *
import asyncio


args = Args()

cstring = TelegramClient('session', api_id=args.api_id, api_hash=args.api_hash)
client = cstring.start(bot_token=args.api_key)
loop = asyncio.get_event_loop()
me = loop.run_until_complete(client.get_me())
username = me.username
